package com.todo.mytodolist.controllers;

import com.todo.mytodolist.exceptions.BadCredentialsException;
import com.todo.mytodolist.exceptions.ErrorResponse;
import com.todo.mytodolist.exceptions.NoSuchUserException;
import com.todo.mytodolist.exceptions.UserExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class UserControllerAdvice {
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Object> noSuchUser(BadCredentialsException ex) {
        log.info(ex.getMessage());
        HttpStatus responseCode = HttpStatus.UNAUTHORIZED;
        ErrorResponse errorResponse = new ErrorResponse(responseCode.value(), ex.getMessage());
        return new ResponseEntity<>(errorResponse, responseCode);
    }

    @ExceptionHandler(UserExistsException.class)
    public ResponseEntity<Object> noSuchUser(UserExistsException ex) {
        log.info(ex.getMessage());
        HttpStatus responseCode = HttpStatus.CONFLICT;
        ErrorResponse errorResponse = new ErrorResponse(responseCode.value(), ex.getMessage());
        return new ResponseEntity<>(errorResponse, responseCode);
    }

    @ExceptionHandler(NoSuchUserException.class)
    public ResponseEntity<Object> noSuchUser(NoSuchUserException ex) {
        log.info(ex.getMessage());
        HttpStatus responseCode = HttpStatus.NO_CONTENT;
        ErrorResponse errorResponse = new ErrorResponse(responseCode.value(), ex.getMessage());
        return new ResponseEntity<>(errorResponse, responseCode);
    }

//    @ExceptionHandler(NoSuchUserException.class)
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    public ErrorResponse noSuchUser(NoSuchUserException ex) {
//        log.info(ex.getMessage());
//        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getMessage());
//        return errorResponse;
//    }
}
