package com.todo.mytodolist.controllers;

import com.todo.mytodolist.dto.UserDto;
import com.todo.mytodolist.exceptions.BadCredentialsException;
import com.todo.mytodolist.exceptions.NoSuchUserException;
import com.todo.mytodolist.exceptions.UserExistsException;
import com.todo.mytodolist.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDto> getUsers() {
        return userService.getAllUsers();
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addUser(@RequestBody UserDto userDto) throws UserExistsException {
        userService.addUser(userDto);
    }

    @PutMapping(value = "/changePassword",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void changePassword(@RequestBody String username, @RequestBody String password, @RequestBody String newPassword)
            throws NoSuchUserException, BadCredentialsException {
        userService.changeUserPassword(username, password, newPassword);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUser(@PathVariable int id) throws NoSuchUserException {
        return userService.getUser(id);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
    }
}
