package com.todo.mytodolist.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PriorityEnum {
    HIGH("High"),
    NORMAL("Normal"),
    LOW("Low");


    private final String name;

}
