package com.todo.mytodolist.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum StatusEnum {

    NEW("New"),
    IN_PROGRESS("In progress"),
    ON_HOLD("On hold"),
    FINISH("Finish");

    private final String description;


}

