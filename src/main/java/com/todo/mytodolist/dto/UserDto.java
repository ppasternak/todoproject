package com.todo.mytodolist.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    Integer id;
    String username;
    String password;
    Date createdAt;
    Date modifiedAt;
}
