package com.todo.mytodolist.entities;

import com.todo.mytodolist.enums.PriorityEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@NoArgsConstructor
public class Priority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Enumerated(EnumType.STRING)
    PriorityEnum name;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "priorities")
    List<Task> tasks;

}
