package com.todo.mytodolist.repositories;

import com.todo.mytodolist.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    public Optional<User> findUserByUsername(String username);
}
