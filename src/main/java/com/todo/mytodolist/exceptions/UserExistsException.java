package com.todo.mytodolist.exceptions;

public class UserExistsException extends Exception {
    public UserExistsException() {
        super("User exists.");
    }

    public UserExistsException(String message) {
        super(message);
    }
}
