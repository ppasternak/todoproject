package com.todo.mytodolist.exceptions;

public class BadCredentialsException extends Exception {
    public BadCredentialsException() {
        super("Bad username or password.");
    }
}
