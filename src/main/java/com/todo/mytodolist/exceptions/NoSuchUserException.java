package com.todo.mytodolist.exceptions;

public class NoSuchUserException extends Exception {

    public NoSuchUserException() {
        super("User not found.");
    }

    public NoSuchUserException(String message) {
        super(message);
    }
}
