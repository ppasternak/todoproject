package com.todo.mytodolist.services;

import com.todo.mytodolist.dto.UserDto;
import com.todo.mytodolist.entities.User;
import com.todo.mytodolist.exceptions.BadCredentialsException;
import com.todo.mytodolist.exceptions.NoSuchUserException;
import com.todo.mytodolist.exceptions.UserExistsException;
import com.todo.mytodolist.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ModelMapper modelMapper;

    public List<UserDto> getAllUsers() {
        ArrayList<UserDto> userList = new ArrayList<>();
        userRepository.findAll().forEach(user -> userList.add(convertToDto(user)));
        return userList;
    }

    public UserDto getUserByUsername(String username) throws NoSuchUserException {
        Optional<User> userOptional = userRepository.findUserByUsername(username);
        if (userOptional.isPresent()) {
            return convertToDto(userOptional.get());
        } else {
            throw new NoSuchUserException("User not found for username=" + username);
        }
    }

    public UserDto getUser(int id) throws NoSuchUserException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return convertToDto(optionalUser.get());
        } else {
            throw new NoSuchUserException("User not found for id=" + id);
        }
    }

    public void addUser(UserDto userDto) throws UserExistsException {
        try {
            userRepository.save(convertToEntity(userDto));
        } catch (DataIntegrityViolationException ex) {
            throw new UserExistsException();
        }
    }

    public void addUser(String username, String password) throws UserExistsException {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException ex) {
            throw new UserExistsException();
        }
    }

    public boolean changeUserPassword(String username, String password, String newPassword) throws NoSuchUserException, BadCredentialsException {
        Optional<User> userOptional = userRepository.findUserByUsername(username);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (authenticateUser(username, password, user)) {
                user.setPassword(newPassword);
                user.setModifiedAt(new Date());
                userRepository.save(user);
                return true;
            } else {
                throw new BadCredentialsException();
            }
        } else {
            throw new NoSuchUserException();
        }
    }

    private boolean authenticateUser(String username, String password, User user) {
        return user.getUsername().equals(username) && user.getPassword().equals(password);
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }

    private User convertToEntity(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        return user;
    }

}
